## Gift-Card Workbench 🎁🪪

This project have the goal of making and innovative platform for searching gift-cards and giving it to the ones you want

## Project structure 🕍

We get three main folders

- One for the shortener config
- One for the backend
- One for the frontend 
