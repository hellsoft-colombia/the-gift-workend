package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		log.Fatal().Err(err).Msg("Failed to execute application")
	}
}
