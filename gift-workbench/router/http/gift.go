package http

import "github.com/gin-gonic/gin"

type GiftController struct{}

// NewGiftController creates a new gift controller with dependencies
func NewGiftController() *GiftController {
	return &GiftController{}
}

// RegisterRoutes registers the routes of the gift controller
func (c *GiftController) RegisterRoutes(router gin.IRouter) {
	// TODO: Define real routes this is just a sample
	router.GET("/gift", c.getGift)
}

func (c *GiftController) getGift(ctx *gin.Context) {
	ctx.JSON(
		200, gin.H{
			"message": "Merry Christmas! 🎄",
		},
	)
}
