package repository

import (
	"context"

	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/domain/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type CampaignRepository struct {
	Collection *mongo.Collection
}

// NewCampaignRepository creates a new campaign repository with dependencies
func NewCampaignRepository(database *mongo.Database) *CampaignRepository {
	return &CampaignRepository{database.Collection("Campaign")}
}

func (r *CampaignRepository) Campaign(name string) (model.Campaign, error) {
	result := model.Campaign{}
	err := r.Collection.FindOne(context.TODO(), bson.D{{"Name", name}}).Decode(&result)
	if err != nil {
		return model.Campaign{}, err
	}
	return result, nil
}
