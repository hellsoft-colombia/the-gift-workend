package model

type Delivery int32

const (
	ONETOONE Delivery = 1 << iota
	RANDOM   Delivery = 1 << iota
)

type Campaign struct {
	Name         string
	Owner        string
	MessageTitle string
	MessageBody  string
	Receivers    []Person
	Gifts        []Gift
	Delivery     Delivery
}
