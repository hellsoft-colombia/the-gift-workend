package model

type Gift struct {
	Type     string `json:"type"`
	Vendor   string `json:"vendor"`
	Code     string `json:"code,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}
