package model

type Preference int32

const (
	SMS      Preference = 1 << iota
	WHATSAPP Preference = 1 << iota
)

type Person struct {
	Name              string
	Mobile            string
	Email             string
	CountryCode       string
	ContactPreference Preference
}
