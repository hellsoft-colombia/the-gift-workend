package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/tavsec/gin-healthcheck/checks"
	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/cmd/config"
	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/cmd/setup"
	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/cmd/setup/gincheckers"
	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/repository"
	"gitlab.com/hellsoft-colombia/the-gift-workend/gift-workbench/router/http"
)

type startCmd struct {
}

// Execute runs the application
func (c *startCmd) Execute() error {
	log.Info().Msg("Starting gift workbench application...")

	conf, err := config.LoadConfigurationFromEnv(configFile)
	if err != nil {
		log.Err(err).Msg("Failed to load configuration from environment")
		return err
	}

	// Add the initial configuration
	loadLoggerConfig(conf.AppConfig.Env)

	// TODO: Add the production configuration based on the environment
	if conf.AppConfig.Env == "prod" {
		gin.SetMode(gin.ReleaseMode)
	}

	// Initialize repositories
	mongo, shutdownFn, err := setup.SetupMongo(conf.Mongo.URL, conf.Mongo.DbName)
	if err != nil {
		log.Err(err).Msg("Failed to connect the mongo client")
		return err
	}

	defer func() {
		if err = shutdownFn(context.Background()); err != nil {
			log.Fatal().Err(err).Msg("Failed to shutdown mongo client")
		}
	}()

	campaignRepo := repository.NewCampaignRepository(mongo.Database)

	// TODO: Add the logic to the service to receive a repository
	_ = campaignRepo

	// Initialize controllers
	giftController := http.NewGiftController()

	healthChks := []checks.Check{gincheckers.NewMongoChecker(mongo.Client)}
	ginRouter := setup.SetupGinRouter(healthChks, giftController)

	log.Info().Msg("Starting gin server...")
	ginPort := fmt.Sprintf(":%d", conf.Gin.Port)
	if err := ginRouter.Run(ginPort); err != nil {
		log.Fatal().Err(err).Msg(fmt.Sprintf("Failed to start gin server on port %s", ginPort))
	}

	return nil
}

func newStartCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "start",
		Short: "Start the application",
		RunE: func(cmd *cobra.Command, args []string) error {
			command := &startCmd{}

			return command.Execute()
		},
	}

	return cmd
}

func loadLoggerConfig(env string) {
	if env == "local" {
		output := &zerolog.ConsoleWriter{
			Out:        os.Stdout,
			NoColor:    false,
			TimeFormat: "2006-01-02 15:04:05",
		}

		log.Logger = zerolog.New(output).With().Timestamp().Logger()

		return
	}
}
