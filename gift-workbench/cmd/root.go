package cmd

import (
	"github.com/common-nighthawk/go-figure"
	"github.com/spf13/cobra"
)

var (
	rootCmd = &cobra.Command{
		Use:   "rails.ach-window-manager",
		Short: "Simple ACH Window manager",
	}

	configFile string
)

func init() {
	initFlags()

	// In this section we add the commands for the application
	srtCmd := newStartCmd()
	rootCmd.AddCommand(srtCmd)
}

func initFlags() {
	// Add the configuration flags to the root command, this will be used by all the subcommands
	rootCmd.PersistentFlags().StringVar(
		&configFile, "config",
		"./config.yaml", "config file (default is $PWD/config.yaml)",
	)
}

// Execute executes the root command of the application
func Execute() error {
	figure.NewColorFigure("Gift Workbench", "doom", "blue", false).Print()

	return rootCmd.Execute()
}
