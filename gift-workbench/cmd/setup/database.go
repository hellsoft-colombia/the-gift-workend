package setup

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ShutDownFn func(context.Context) error

type Mongo struct {
	Client   *mongo.Client
	Database *mongo.Database
}

// SetupMongo creates a new mongo client and returns a shutdown function
func SetupMongo(dataSource, dbName string) (*Mongo, ShutDownFn, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(
		ctx, options.Client().ApplyURI(
			dataSource,
		),
	)

	err = client.Ping(ctx, nil)

	if err != nil {
		return nil, nil, err
	}

	shutdownFn := func(ctx context.Context) error {
		return client.Disconnect(ctx)
	}

	return &Mongo{
		Client:   client,
		Database: client.Database(dbName),
	}, shutdownFn, nil
}
