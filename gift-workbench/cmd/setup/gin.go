package setup

import (
	"github.com/dn365/gin-zerolog"
	"github.com/gin-gonic/gin"
	healthcheck "github.com/tavsec/gin-healthcheck"
	"github.com/tavsec/gin-healthcheck/checks"
	"github.com/tavsec/gin-healthcheck/config"
)

type domainRegister interface {
	RegisterRoutes(router gin.IRouter)
}

// SetupGinRouter sets up the gin router with the middlewares and routes
func SetupGinRouter(healthChk []checks.Check, registerers ...domainRegister) *gin.Engine {
	router := gin.New()

	// Add the router middlewares
	router.Use(ginzerolog.Logger("gin"))
	router.Use(gin.Recovery())

	// Add the router routes here
	for _, registerer := range registerers {
		registerer.RegisterRoutes(router.Group("/api/v1"))
	}

	healthcheck.New(router, config.DefaultConfig(), healthChk)

	return router
}
