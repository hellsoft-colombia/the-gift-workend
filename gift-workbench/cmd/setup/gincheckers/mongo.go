package gincheckers

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

type MongoChecker struct {
	client *mongo.Client
}

// NewMongoChecker creates a new mongo checker
func NewMongoChecker(client *mongo.Client) *MongoChecker {
	return &MongoChecker{client: client}
}

// Pass checks if the mongo client is connected
func (c *MongoChecker) Pass() bool {
	return c.client.Ping(context.Background(), nil) == nil
}

// Name returns the name of the checker
func (c *MongoChecker) Name() string {
	return "Mongo DB"
}
