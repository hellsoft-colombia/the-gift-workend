package config

import (
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"
)

// Gin holds the configuration for the gin server
type Gin struct {
	Port int `mapstructure:"port" validate:"required"`
}

// Mongo holds the configuration for the database connection
type Mongo struct {
	URL    string `mapstructure:"url"`
	DbName string `mapstructure:"database_name"`
}

// AppConfig stores the basic application configuration
type AppConfig struct {
	Env string `mapstructure:"env"`
}

// Configuration holds the application configuration
type Configuration struct {
	AppConfig AppConfig `mapstructure:"app_config"`
	Gin       Gin       `mapstructure:"gin" validate:"required"`
	Mongo     Mongo     `mapstructure:"mongo" validate:"required"`
}

// LoadConfigurationFromEnv takes the config from environment variables and file, then loads into the application structure
func LoadConfigurationFromEnv(path string) (*Configuration, error) {
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.SetConfigFile(path)
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	var config Configuration
	if err := viper.Unmarshal(&config); err != nil {
		return nil, err
	}

	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	return &config, nil
}
